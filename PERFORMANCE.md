


#### 2p setup phone + PC

###### Phone - Alice
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     301 ms   |


###### Phone - Bob
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     335 ms   |



#### 2p sign phone + PC

###### Phone - Alice
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     123 ms   |


###### Phone - Bob
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     121 ms   |



#### mp setup

###### 2-2 (phone + pc)
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     268 ms   |


###### 2-5 (phone + pc + 3 google VMs)
|             |  4. 11. 2020 |
| :---        |    :----:    |
| Time        |     409 ms   |

###### 5-5 (phone + pc + 3 google VMs)
|             |  4. 11. 2020 |
| :---        |    :----:    |
| Time        |     321 ms   |



#### 2p threshold sign 

###### phone + pc
|             |  3. 11. 2020 | 18. 11. 2020 | 18. 11. 2020 |
| :---        |    :----:    |    :----:    |    :----:    |
| Time        |     113 ms   |     112 ms   |     114 ms   |



#### mp threshold sign

###### Threshold size: 2 (phone + pc)
|             | 18. 11. 2020 | 18. 11. 2020 |
| :---        |    :----:    |    :----:    |
| Time        |     154 ms   |     138 ms   |

###### Threshold size: 5 (phone + pc + 3 google VMs)
|             |  3. 11. 2020 |
| :---        |    :----:    |
| Time        |     238 ms   |