extern crate cc;

fn main() {
    cc::Build::new()
        .file("src/sha256_octa.c")
        .compile("sha256_octa");
}
