#![allow(non_snake_case)]

use time::Instant;
use std::ffi::{CString, CStr};
use jni::JNIEnv;
use jni::objects::{JObject, JString/*, JClass*/};
use jni::sys::{jstring/*, jbyteArray*/, jint};

use std::time::Duration;
use std::thread::sleep;
use std::net::{TcpListener, TcpStream};

use std::convert::TryInto;

extern crate rand;
use rand::{Rng};

extern crate time;

use curves::Ford;
use curves::f_4141::FSecp256Ord;


use super::*;

fn bench2p(ip: &str, alice: bool, setup: bool) -> String {

    let msg = "this is a test".as_bytes();

	let (mut streamrecv, mut streamsend) = if ip == "0.0.0.0" {
		    let port = "0.0.0.0:12345";
			let listener = TcpListener::bind(port).unwrap_or_else(|e| {panic!(e) });
	        let (stream1, _) = listener.accept().unwrap_or_else(|e| {panic!(e) });
	
	        let stream2 = stream1.try_clone().unwrap();
	        (stream2, stream1)
	} else {
		    let port = format!("{}:12345", ip);
	        let stream1 = TcpStream::connect(port).unwrap();
	        let stream2 = stream1.try_clone().unwrap();
	        (stream1, stream2)
	};

	streamsend.set_nodelay(true).expect("Could not set nodelay");
	streamrecv.set_nodelay(true).expect("Could not set nodelay");
	let iters = 1000;
	let mut seeder = rand::os::OsRng::new().unwrap();
	let mut rng = rand::ChaChaRng::new_unseeded();
	rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

	if alice {
		let ska = FSecp256Ord::from_slice(&[0xc93d9fa738a8b4b6, 0xe8dd5f4af65e7462, 0xcbdf97aeca50c5c4, 0x67498f7dcab40d3]);
        if setup {
            let signstart = Instant::now();
            for _ in 0..iters {
                mpecdsa::Alice2P::new(&ska, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
            }
            let signend = Instant::now();
            return format!("{:.3} ms avg", ((signend - signstart).whole_milliseconds() as f64)/(iters as f64));

        } else  {
        	let alice = mpecdsa::Alice2P::new(&ska, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
        	let signstart = Instant::now();
        	for _ in 0..iters {
            	alice.sign(&msg, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
        	}
        	let signend = Instant::now();
        
        	return format!("{:.3} ms avg", ((signend - signstart).whole_milliseconds() as f64)/(iters as f64));
        }
    } else {
    	let skb = FSecp256Ord::from_slice(&[0xb75db4463a602ff0, 0x83b6a76e7fad1ec, 0xa33f33b8e9c84dbd, 0xb94fceb9fff7cfb2]);

        if setup {
            let signstart = Instant::now();
            for _ in 0..iters {
                mpecdsa::Bob2P::new(&skb, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
            }
            let signend = Instant::now();
            return format!("{:.3} ms avg", ((signend - signstart).whole_milliseconds() as f64)/(iters as f64));

        } else {

    		let bob = mpecdsa::Bob2P::new(&skb, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
        	let signstart = Instant::now();
        	for _ in 0..iters {
            	bob.sign(&msg, &mut rng, &mut streamrecv, &mut streamsend).unwrap();
        	}
        	let signend = Instant::now();
        
        	return format!("{:.3} ms avg", ((signend - signstart).whole_milliseconds() as f64)/(iters as f64));
        }
    }
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_bench2pSignAlice(env: JNIEnv, _: JObject, j_ip: JString) -> jstring {
	let ip = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    );
    let output = env.new_string(bench2p(ip.to_str().unwrap(), true, false)).unwrap();
    output.into_inner()
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_bench2pSignBob(env: JNIEnv, _: JObject, j_ip: JString) -> jstring {
        let ip = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    );
    let output = env.new_string(bench2p(ip.to_str().unwrap(), false, false)).unwrap();
    output.into_inner()
    
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_bench2pSetupAlice(env: JNIEnv, _: JObject, j_ip: JString) -> jstring {
    let ip = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    );
    let output = env.new_string(bench2p(ip.to_str().unwrap(), true, true)).unwrap();
    output.into_inner()
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_bench2pSetupBob(env: JNIEnv, _: JObject, j_ip: JString) -> jstring {
    	let ip = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    );
    let output = env.new_string(bench2p(ip.to_str().unwrap(), false, true)).unwrap();
    output.into_inner()
	
}

fn benchMpSign(ip: String, parties: usize, index: usize, port: usize, thresh2mp: bool) -> String {

    let mut sendvec:Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);
    let mut recvvec: Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);


    let addrs: Vec<&str> = ip.split(",").collect();

    let min_ports = parties;
    let mut ports = Vec::with_capacity(min_ports);
    for ii in port..(port+min_ports) {
        ports.push(format!("{}", ii));
    }
    for jj in 0..parties {
    	if jj < index {
                let port_index = jj;
                let port = format!("0.0.0.0:{}", &ports[port_index]);
                println!("{} waiting for {} to connect on {}", index, jj, port);
                let listener = TcpListener::bind(port).unwrap_or_else(|e| { panic!(e) });
                let (recv, _) = listener.accept().unwrap_or_else(|e| {panic!(e)} );
                let send = recv.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else if jj > index {
                let port_index = index;
                let port = format!("{}:{}", addrs[jj], &ports[port_index]);
                println!("{} connecting to {} server {:?}...", index, jj, port);
                let mut send = TcpStream::connect(&port);
                let connection_wait_time = 2*60;
                let poll_interval = 100;
                for _ in 0..(connection_wait_time*1000/poll_interval) {
                    if send.is_err() {
                        sleep(Duration::from_millis(poll_interval));
                        send = TcpStream::connect(&port);    
                    }
                }
                let send = send.unwrap();
                let recv = send.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else {
                // pause here so the lower numbers can start their listeners?
                //sleep(Duration::from_millis(500));
                sendvec.push(None);
                recvvec.push(None);
            }
    }
    
    let iters = 1000;
    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

    if index==parties-1 {
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to read ready signal.", index));
    }

    println!("{} connected. Initializing...", index);

    let mut signer = mpecdsa::ThresholdSigner::new(index, parties, &mut rng, sendvec.as_mut_slice(), recvvec.as_mut_slice()).unwrap();

    if index==parties-1 {
        let mut sigread = [1u8; 1];
        for ii in 0..parties-1 {
            recvvec[ii].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", ii));
        }
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        sendvec[parties-1].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
        sendvec[parties-1].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", parties-1));
    }

    println!("Performing {} Iteration Benchmark...", iters);
    let msg = &"etaoin shrdlu".as_bytes();
    let mut z = [0; HASH_SIZE];
	ecdsa_hash(&mut z, msg);
    let signstart = Instant::now();
    if parties == 2 && thresh2mp {
    	for _ in 0..iters {

			signer.sign_threshold(&(0usize..parties).collect::<Vec<usize>>(), &mut z, &mut rng, &mut recvvec, &mut sendvec).unwrap();
    	}

    } else {

    	for _ in 0..iters {
        	signer.sign(&(0usize..index).chain((index+1)..(parties)).collect::<Vec<usize>>(), &"etaoin shrdlu".as_bytes(), &mut rng, &mut recvvec, &mut sendvec).unwrap();
    	}
    }
    let signend = Instant::now();
    format!("{:.3} ms avg", ((signend - signstart).whole_milliseconds() as f64)/(iters as f64))
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_benchMpSign2pMp(env: JNIEnv, _: JObject, j_ip: JString, j_parties: jint, j_index: jint, j_port: jint) -> jstring {
	let parties: usize = j_parties.try_into().unwrap();

    let index = j_index.try_into().unwrap();

    let addrs: String = CString::from( CStr::from_ptr(env.get_string(j_ip).unwrap().as_ptr())).into_string().unwrap();
    let port: usize = j_port.try_into().unwrap();

    let output = env.new_string(benchMpSign(addrs, parties, index, port, true)).unwrap();
    output.into_inner()
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_benchMpSign(env: JNIEnv, _: JObject, j_ip: JString, j_parties: jint, j_index: jint, j_port: jint) -> jstring {
		let parties: usize = j_parties.try_into().unwrap();

    let index = j_index.try_into().unwrap();

    let addrs: String = CString::from( CStr::from_ptr(env.get_string(j_ip).unwrap().as_ptr())).into_string().unwrap();
    let port: usize = j_port.try_into().unwrap();

    let output = env.new_string(benchMpSign(addrs, parties, index, port, false)).unwrap();
    output.into_inner()
}


#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_benchMpSetup(env: JNIEnv, _: JObject, j_ip: JString, j_parties: jint, j_index: jint, j_port: jint, j_thres: jint) -> jstring {
    let parties: usize = j_parties.try_into().unwrap();
    let index = j_index.try_into().unwrap();
    let thres = j_thres.try_into().unwrap();

    let mut sendvec:Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);
    let mut recvvec: Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);

    let addrs: String = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    ).into_string().unwrap();
    let addrs: Vec<&str> = addrs.split(",").collect();
    let port: usize = j_port.try_into().unwrap();
    let min_ports = parties;
    let mut ports = Vec::with_capacity(min_ports);
    for ii in port..(port+min_ports) {
        ports.push(format!("{}", ii));
    }
    for jj in 0..parties {
        if jj < index {
            let port_index = jj;
            let port = format!("0.0.0.0:{}", &ports[port_index]);
            println!("{} waiting for {} to connect on {}", index, jj, port);
            let listener = TcpListener::bind(port).unwrap_or_else(|e| { panic!(e) });
            let (recv, _) = listener.accept().unwrap_or_else(|e| {panic!(e)} );
            let send = recv.try_clone().unwrap();
            recv.set_nodelay(true).expect("Could not set nodelay");
            send.set_nodelay(true).expect("Could not set nodelay");
            sendvec.push(Some(send));
            recvvec.push(Some(recv));
        } else if jj > index {
            let port_index = index;
            let port = format!("{}:{}", addrs[jj], &ports[port_index]);
            println!("{} connecting to {} server {:?}...", index, jj, port);
            let mut send = TcpStream::connect(&port);
            let connection_wait_time = 2*60;
            let poll_interval = 100;
            for _ in 0..(connection_wait_time*1000/poll_interval) {
                if send.is_err() {
                    sleep(Duration::from_millis(poll_interval));
                    send = TcpStream::connect(&port);    
                }
            }
            let send = send.unwrap();
            let recv = send.try_clone().unwrap();
            recv.set_nodelay(true).expect("Could not set nodelay");
            send.set_nodelay(true).expect("Could not set nodelay");
            sendvec.push(Some(send));
            recvvec.push(Some(recv));
        } else {
            // pause here so the lower numbers can start their listeners?
            //sleep(Duration::from_millis(500));
            sendvec.push(None);
            recvvec.push(None);
        }
    }
    
    let iters = 1000;
    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

    println!("{} connected. Initializing...", index);

    if index==parties-1 {
        let mut sigread = [1u8; 1];
        for ii in 0..parties-1 {
            recvvec[ii].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", ii));
        }
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        sendvec[parties-1].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
        sendvec[parties-1].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", parties-1));
    }

    println!("Performing {} Iteration Benchmark...", iters);

    let setupstart = Instant::now();
    for _ in 0..iters {
        mpecdsa::ThresholdSigner::new(index, thres, &mut rng, sendvec.as_mut_slice(), recvvec.as_mut_slice()).unwrap();
    }
    let setupend = Instant::now();
    let output = env.new_string(format!("{:.3} ms avg", ((setupend - setupstart).whole_milliseconds() as f64)/(iters as f64))).unwrap();
    output.into_inner()
}

fn benchMpSignPrecomp(ip: String, parties: usize, index: usize, port: usize) -> String {

    let mut sendvec:Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);
    let mut recvvec: Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);


    let addrs: Vec<&str> = ip.split(",").collect();

    let min_ports = parties;
    let mut ports = Vec::with_capacity(min_ports);
    for ii in port..(port+min_ports) {
        ports.push(format!("{}", ii));
    }
    for jj in 0..parties {
    	if jj < index {
                let port_index = jj;
                let port = format!("0.0.0.0:{}", &ports[port_index]);
                println!("{} waiting for {} to connect on {}", index, jj, port);
                let listener = TcpListener::bind(port).unwrap_or_else(|e| { panic!(e) });
                let (recv, _) = listener.accept().unwrap_or_else(|e| {panic!(e)} );
                let send = recv.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else if jj > index {
                let port_index = index;
                let port = format!("{}:{}", addrs[jj], &ports[port_index]);
                println!("{} connecting to {} server {:?}...", index, jj, port);
                let mut send = TcpStream::connect(&port);
                let connection_wait_time = 2*60;
                let poll_interval = 100;
                for _ in 0..(connection_wait_time*1000/poll_interval) {
                    if send.is_err() {
                        sleep(Duration::from_millis(poll_interval));
                        send = TcpStream::connect(&port);    
                    }
                }
                let send = send.unwrap();
                let recv = send.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else {
                // pause here so the lower numbers can start their listeners?
                //sleep(Duration::from_millis(500));
                sendvec.push(None);
                recvvec.push(None);
            }
    }
    
    let iters = 1000;
    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

    if index==parties-1 {
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to read ready signal.", index));
    }

    println!("{} connected. Initializing...", index);

    let mut signer = mpecdsa::ThresholdSigner::new(index, parties, &mut rng, sendvec.as_mut_slice(), recvvec.as_mut_slice()).unwrap();

    if index==parties-1 {
        let mut sigread = [1u8; 1];
        for ii in 0..parties-1 {
            recvvec[ii].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", ii));
        }
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        sendvec[parties-1].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
        sendvec[parties-1].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", parties-1));
    }

    println!("Performing {} Iteration Benchmark...", iters);
    let msg = &"etaoin shrdlu".as_bytes();
    let mut z = [0; HASH_SIZE];
	ecdsa_hash(&mut z, msg);
	let counterparties = &(0usize..parties).collect::<Vec<usize>>();
    let mut prunedrecv = recvvec.iter_mut().enumerate().filter_map(|(index, val)| if counterparties.contains(&index) {Some(val)} else {None}).collect();
	let mut prunedsend = sendvec.iter_mut().enumerate().filter_map(|(index, val)| if counterparties.contains(&index) {Some(val)} else {None}).collect();
		


	let precompstart = Instant::now();
	for _ in 0..iters {
		signer.sign_threshold_precomp(counterparties, &mut rng, &mut prunedrecv, &mut prunedsend).unwrap();
	} 
	let precompend = Instant::now();
	let (vi, wiaug, rx) = signer.sign_threshold_precomp(counterparties, &mut rng, &mut prunedrecv, &mut prunedsend).unwrap();
	let signstart = Instant::now();
	for _ in 0..iters {
		signer.sign_threshold_finish(vi, wiaug, rx, &mut z, counterparties, &mut prunedrecv, &mut prunedsend).unwrap();
	}
	let signend = Instant::now();
    
    format!("precomp:{:.3} sign: {:.3} ms avg", ((precompend - precompstart).whole_milliseconds() as f64)/(iters as f64), ((signend - signstart).whole_milliseconds() as f64)/(iters as f64))
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_benchMpSignPrecomp(env: JNIEnv, _: JObject, j_ip: JString, j_parties: jint, j_index: jint, j_port: jint) -> jstring {
	let parties: usize = j_parties.try_into().unwrap();

    let index = j_index.try_into().unwrap();

    let addrs: String = CString::from( CStr::from_ptr(env.get_string(j_ip).unwrap().as_ptr())).into_string().unwrap();
    let port: usize = j_port.try_into().unwrap();

    let output = env.new_string(benchMpSignPrecomp(addrs, parties, index, port)).unwrap();
    output.into_inner()
}
