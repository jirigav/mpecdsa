#![allow(non_snake_case)]

use std::ffi::{CString, CStr};
use jni::JNIEnv;
use jni::objects::{JObject, JString/*, JClass*/};
use jni::sys::{jbyteArray, jint};

use std::time::Duration;
use std::thread::sleep;
use std::net::{TcpListener, TcpStream};
use std::str::FromStr;
use std::convert::TryInto;

extern crate rand;
use rand::{Rng};

extern crate time;

use curves::{ECGroup, Secp, SecpOrd};

use std::fs::File;
use std::io::prelude::*;
use std::io::Read;

use super::*;

fn get_parties_index_and_threshold(path: &str) -> (usize, usize, usize) {
	let mut file = File::open(path).unwrap();

	let mut parties_buffer = [0u8; 1];
	file.read(&mut parties_buffer).unwrap();
	let parties: usize = parties_buffer[0].try_into().unwrap();
	
	let mut index_buffer = [0u8; 1];
	file.read(&mut index_buffer).unwrap();
	let index: usize = index_buffer[0].try_into().unwrap();

	let mut threshold_buffer = [0u8; 1];
	file.read(&mut threshold_buffer).unwrap();
	let threshold: usize = threshold_buffer[0].try_into().unwrap();

	(parties, index, threshold)
}

fn save_signer(signer: mpecdsa::ThresholdSigner, parties: usize, path: &str) {
    let mut file = File::create(path).unwrap();

    file.write_all(&[parties as u8]).unwrap(); // parties

    file.write_all(&[*signer.playerindex() as u8]).unwrap(); // index

    file.write_all(&[*signer.threshold() as u8]).unwrap(); // threshold size

    let mut sk = [0u8; SecpOrd::NBYTES];
    signer.poly_point().to_bytes(&mut sk);
    file.write_all(&sk).unwrap();  // sk
    
    let mut point_com_raw = [0u8; Secp::NBYTES];
    signer.pk().to_bytes(&mut point_com_raw);
    file.write_all(&point_com_raw).unwrap();  // pk
}

#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_mpSetup(env: JNIEnv, _: JObject, j_ip: JString, j_port: jint, j_parties: jint, j_index: jint, j_thres: jint, j_path: JString) -> jbyteArray {
    let parties: usize = j_parties.try_into().unwrap();
    let index = j_index.try_into().unwrap();
    let thres = j_thres.try_into().unwrap();

    let mut sendvec:Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);
    let mut recvvec: Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);

    let path: String = CString::from(CStr::from_ptr(env.get_string(j_path).unwrap().as_ptr())).into_string().unwrap();

    let addrs: String = CString::from(CStr::from_ptr(env.get_string(j_ip).unwrap().as_ptr())).into_string().unwrap();
    let addrs: Vec<&str> = addrs.split(",").collect();
    let port: usize = j_port.try_into().unwrap();
    let min_ports = parties;
    let mut ports = Vec::with_capacity(min_ports);
    for ii in port..(port+min_ports) {
        ports.push(format!("{}", ii));
    }
    for jj in 0..parties {
        if jj < index {
            let port_index = jj;
            let port = format!("0.0.0.0:{}", &ports[port_index]);
            println!("{} waiting for {} to connect on {}", index, jj, port);
            let listener = TcpListener::bind(port).unwrap_or_else(|e| { panic!(e) });
            let (recv, _) = listener.accept().unwrap_or_else(|e| {panic!(e)} );
            let send = recv.try_clone().unwrap();
            recv.set_nodelay(true).expect("Could not set nodelay");
            send.set_nodelay(true).expect("Could not set nodelay");
            sendvec.push(Some(send));
            recvvec.push(Some(recv));
        } else if jj > index {
            let port_index = index;
            let port = format!("{}:{}", addrs[jj], &ports[port_index]);
            println!("{} connecting to {} server {:?}...", index, jj, port);
            let mut send = TcpStream::connect(&port);
            let connection_wait_time = 2*60;
            let poll_interval = 100;
            for _ in 0..(connection_wait_time*1000/poll_interval) {
                if send.is_err() {
                    sleep(Duration::from_millis(poll_interval));
                    send = TcpStream::connect(&port);    
                }
            }
            let send = send.unwrap();
            let recv = send.try_clone().unwrap();
            recv.set_nodelay(true).expect("Could not set nodelay");
            send.set_nodelay(true).expect("Could not set nodelay");
            sendvec.push(Some(send));
            recvvec.push(Some(recv));
        } else {
            // pause here so the lower numbers can start their listeners?
            //sleep(Duration::from_millis(500));
            sendvec.push(None);
            recvvec.push(None);
        }
    }
    
    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

    println!("{} connected. Initializing...", index);

    if index==parties-1 {
        let mut sigread = [1u8; 1];
        for ii in 0..parties-1 {
            recvvec[ii].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", ii));
        }
        for ii in 0..parties-1 {
            sendvec[ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
            sendvec[ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        }
    } else {
        let mut sigread = [1u8; 1];
        sendvec[parties-1].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
        sendvec[parties-1].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", parties-1));
    }

    
    let signer = mpecdsa::ThresholdSigner::new(index, thres, &mut rng, sendvec.as_mut_slice(), recvvec.as_mut_slice()).unwrap();

    let mut pk = [0u8; Secp::NBYTES];
    signer.pk().to_bytes(&mut pk);

    save_signer(signer, parties, &path);

    return JNIEnv::byte_array_from_slice(&env, &pk).unwrap();
}



#[no_mangle]
pub unsafe extern fn Java_mpecdsaDemo_mpSign(env: JNIEnv, _: JObject, j_ip: JString, j_port: jint, j_threshold_indices: JString, j_path: JString, j_msg_hash: jbyteArray) -> jbyteArray {
	let path: String = CString::from(CStr::from_ptr(env.get_string(j_path).unwrap().as_ptr())).into_string().unwrap();

    let threshold_indices: String = CString::from(CStr::from_ptr(env.get_string(j_threshold_indices).unwrap().as_ptr())).into_string().unwrap();
    let msg_hash : &mut [u8] = &mut JNIEnv::convert_byte_array(&env, j_msg_hash).unwrap();

    let indices_vec: Vec<usize> = threshold_indices.split(",").map(|threshold_indices| usize::from_str(&threshold_indices).unwrap()).collect();

	let (parties, index, _threshold) = get_parties_index_and_threshold(&path);

    let mut sendvec:Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);
    let mut recvvec: Vec<Option<std::net::TcpStream>> = Vec::with_capacity(parties);

    let addrs: String = CString::from(
        CStr::from_ptr(
            env.get_string(j_ip).unwrap().as_ptr()
        )
    ).into_string().unwrap();
    let addrs: Vec<&str> = addrs.split(",").collect();
    let port: usize = j_port.try_into().unwrap();
    let min_ports = parties;
    let mut ports = Vec::with_capacity(min_ports);
    for ii in port..(port+min_ports) {
        ports.push(format!("{}", ii));
    }
    for jj in 0..parties {
    	if indices_vec.contains(&jj) && jj < index {
                let port_index = jj;
                let port = format!("0.0.0.0:{}", &ports[port_index]);
                println!("{} waiting for {} to connect on {}", index, jj, port);
                let listener = TcpListener::bind(port).unwrap_or_else(|e| { panic!(e) });
                let (recv, _) = listener.accept().unwrap_or_else(|e| {panic!(e)} );
                let send = recv.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else if indices_vec.contains(&jj) && jj > index {
                let port_index = index;
                let port = format!("{}:{}", addrs[indices_vec.iter().position(|&x| x == jj).unwrap()], &ports[port_index]);
                println!("{} connecting to {} server {:?}...", index, jj, port);
                let mut send = TcpStream::connect(&port);
                let connection_wait_time = 2*60;
                let poll_interval = 100;
                for _ in 0..(connection_wait_time*1000/poll_interval) {
                    if send.is_err() {
                        sleep(Duration::from_millis(poll_interval));
                        send = TcpStream::connect(&port);    
                    }
                }
                let send = send.unwrap();
                let recv = send.try_clone().unwrap();
                recv.set_nodelay(true).expect("Could not set nodelay");
                send.set_nodelay(true).expect("Could not set nodelay");
                sendvec.push(Some(send));
                recvvec.push(Some(recv));
            } else {
                // pause here so the lower numbers can start their listeners?
                //sleep(Duration::from_millis(500));
                sendvec.push(None);
                recvvec.push(None);
            }
    }
    

    let mut seeder = rand::os::OsRng::new().unwrap();
    let mut rng = rand::ChaChaRng::new_unseeded();
    rng.set_counter(seeder.gen::<u64>(), seeder.gen::<u64>());

    if index==parties-1 {
        for ii in &indices_vec {
            if *ii != index {
                sendvec[*ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
                sendvec[*ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
            }
        }
    } else {
        let mut sigread = [1u8; 1];
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to read ready signal.", index));
    }

    println!("{} connected. Initializing...", index);

    let mut signer = mpecdsa::ThresholdSigner::load_signer(&path, &indices_vec, recvvec.as_mut_slice(), sendvec.as_mut_slice(), &mut rng).unwrap(); 

    if index==parties-1 {
        let mut sigread = [1u8; 1];
        for ii in &indices_vec {
            if *ii != index {
                recvvec[*ii].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", ii));
            }
        }
        for ii in &indices_vec {
            if *ii != index {
                sendvec[*ii].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
                sendvec[*ii].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
            }
        }
    } else {
        let mut sigread = [1u8; 1];
        sendvec[parties-1].as_mut().unwrap().write(&[0]).expect(&format!("Party {} failed to send ready signal.", index));
        sendvec[parties-1].as_mut().unwrap().flush().expect(&format!("Party {} failed to flush.", index));
        recvvec[parties-1].as_mut().unwrap().read_exact(&mut sigread).expect(&format!("Party {} failed to send ready signal.", parties-1));
    }


    let (s, r) = signer.sign_threshold(&indices_vec, msg_hash, &mut rng, &mut recvvec, &mut sendvec).unwrap();
    
    let mut bufs = [0u8; 64];
    
    s.to_bytes(&mut bufs[0 .. 32]);
    r.to_bytes(&mut bufs[32 .. 64]);

    return JNIEnv::byte_array_from_slice(&env, &bufs).unwrap();
}